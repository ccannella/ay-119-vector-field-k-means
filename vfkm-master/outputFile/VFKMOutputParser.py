import numpy as np

numClusters = 2
totalError = 0.0
clusterOutputFile = open("clusterResults.txt", "w")
for cluster in range(0, numClusters):
	clusterResultFileName = "curves_r_" + str(cluster) + ".txt"
	clusterResult = np.loadtxt(clusterResultFileName, comments = "#")
	type1Particles = 0
	type2Particles = 0
	for particle in range(0, len(clusterResult)):
		totalError = totalError + clusterResult[particle][1]
		if (clusterResult[particle][0] % 2 == 0):
			type1Particles = type1Particles + 1
		else:
			type2Particles = type2Particles + 1
	clusterOutputFile.write("Cluster " + str(cluster) + ": Type 1- " + str(type1Particles) + " Type 2- " + str(type2Particles) + "\n")
clusterOutputFile.write("Total Error: " + str(totalError))
	