import numpy as np
from matplotlib import pyplot as pl

numClusters = 2
minX = 0
maxX = 100
minY = 0
maxY = 100
vectorFieldFile = open("vf_r.txt")
axisLength = int(np.sqrt(int(vectorFieldFile.readlines()[0])))
xAxis = np.linspace(minX, maxX, axisLength)
yAxis = np.linspace(minY, maxY, axisLength)


for cluster in range(0, numClusters):
	clusterResultFileName = "vf_r_" + str(cluster) + ".txt"
	clusterResult = np.loadtxt(clusterResultFileName, comments = "#", skiprows = 1)
	X = np.zeros([axisLength, axisLength])
	Y = np.zeros([axisLength, axisLength])
	U = np.zeros([axisLength, axisLength])
	V = np.zeros([axisLength, axisLength])
	for x in range(0, axisLength):
		for y in range(0, axisLength):
			U[x][y] = clusterResult[y*axisLength + x][0]
			V[x][y] = clusterResult[y*axisLength + x][1]
			X[x][y] = xAxis[x]
			Y[x][y] = yAxis[y]
	pl.quiver(X, Y, U, V)
	pl.show()
