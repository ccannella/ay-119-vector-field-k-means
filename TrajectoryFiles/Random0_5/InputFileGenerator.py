import numpy as np
from matplotlib import pyplot as pl

numParticles = 100
timeSteps = 100
maxX = 100
minX = 0
maxY = 100
minY = 0
randomMovementChance = 0.5
outputFileName = "VFKMInputRandom" + str(randomMovementChance).replace(".", "_") + ".dat"
outputFile = open(outputFileName, "w")
outputFile.write(str(minX) + " " + str(maxX) + " " + str(minY) + " " + str(maxY) + " " + str(0) + " " + str(timeSteps) + "\n")
for particle in range(0, numParticles):
	type1FileName = "Type1Particle" + str(particle) + "Random" + str(randomMovementChance).replace(".", "_") + ".dat"
	type2FileName = "Type2Particle" + str(particle) + "Random" + str(randomMovementChance).replace(".", "_") + ".dat"
	type1Trajectory = np.loadtxt(type1FileName, comments = "#")
	type2Trajectory = np.loadtxt(type2FileName, comments = "#")
	for index in range(0, len(type1Trajectory)):
		outputFile.write(str(type1Trajectory[index][1]) + " " + str(type1Trajectory[index][2]) + " " + str(type1Trajectory[index][0]) +"\n")
	outputFile.write("0.0 0.0 0.0\n")
	for index in range(0, len(type2Trajectory)):
		outputFile.write(str(type2Trajectory[index][1]) + " " + str(type2Trajectory[index][2]) + " " + str(type2Trajectory[index][0]) +"\n")
	outputFile.write("0.0 0.0 0.0\n")
	pl.plot(type1Trajectory[...,1], type1Trajectory[...,2], color = 'r')
	pl.plot(type2Trajectory[...,1], type2Trajectory[...,2], color = 'b')
pl.show()