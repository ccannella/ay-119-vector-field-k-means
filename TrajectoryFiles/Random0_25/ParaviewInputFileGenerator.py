import numpy as np

numParticles = 100
timeSteps = 100
maxX = 100
maxY = 100
randomMovementChance = 0.25
outputList = []
for particle in range(0, numParticles):
	type1FileName = "Type1Particle" + str(particle) + "Random" + str(randomMovementChance).replace(".", "_") + ".dat"
	type2FileName = "Type2Particle" + str(particle) + "Random" + str(randomMovementChance).replace(".", "_") + ".dat"
	type1Trajectory = np.loadtxt(type1FileName, comments = "#")
	type2Trajectory = np.loadtxt(type2FileName, comments = "#")
	outputList.append([type1Trajectory, 0])
	outputList.append([type2Trajectory, 1])
for time in range(0, timeSteps):
	outputFileName = "Paraview\\ParaviewInputRandom" + str(randomMovementChance).replace(".", "_") + "Time"+ str(time) + ".csv"
	outputFile = open(outputFileName, "w")
	outputFile.write("x,y,class\n")
	for trajectory in outputList:
		if(len(trajectory[0]) > time):
			outputFile.write(str(trajectory[0][time][1]) + "," + str(trajectory[0][time][2]) + "," + str(trajectory[1]) + "\n")