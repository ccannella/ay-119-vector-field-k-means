import numpy as np

numParticles = 100
timeSteps = 100
maxX = 100
minX = 0
maxY = 100
minY = 0
randomMovementChance = 0.1
def particleType1Force(x):
	return np.array([x[0]/np.sqrt(x[0]**2 + x[1]**2), x[1]/np.sqrt(x[0]**2 + x[1]**2)])
def particleType2Force(x):
	return np.array([-1.0*x[0]/np.sqrt(x[0]**2 + x[1]**2), -1.0*x[1]/np.sqrt(x[0]**2 + x[1]**2)])
for particle in range(0, numParticles):
    type1FileName = "Type1Particle" + str(particle) + "Random" + str(randomMovementChance).replace(".", "_") + ".dat"
    type2FileName = "Type2Particle" + str(particle) + "Random" + str(randomMovementChance).replace(".", "_") + ".dat"
    type1File = open(type1FileName, "w")
    type2File = open(type2FileName, "w")
    type1File.write("#t\tx\ty\n")
    type2File.write("#t\tx\ty\n")
    type1ParticlePosition = np.array([np.random.random()*(maxX-minX) + minX, np.random.random()*(maxY-minY) + minY])
    type2ParticlePosition = np.array([np.random.random()*(maxX-minX) + minX, np.random.random()*(maxY-minY) + minY])
    type1OutOfBounds = False
    type2OutOfBounds = False
    for timeStep in range(0, timeSteps):
        if(((type1ParticlePosition[0] < maxX) and (type1ParticlePosition[0] > minX)) and ((type1ParticlePosition[1] < maxY) and (type1ParticlePosition[1] > minY))):
            type1File.write(str(timeStep) + "\t" + str(type1ParticlePosition[0]) + "\t" + str(type1ParticlePosition[1]) + "\n")
            if(np.random.random() < randomMovementChance):
                randomTheta = 2.0*np.pi*np.random.random()
                type1ParticlePosition = np.add(type1ParticlePosition , np.array([np.cos(randomTheta), np.sin(randomTheta)]))
            else:
                type1ParticlePosition = np.add(type1ParticlePosition , particleType1Force(type1ParticlePosition))
        elif(not type1OutOfBounds):
            type1OutOfBounds = True
            if (type1ParticlePosition[0] > maxX):
                type1ParticlePosition[0] = maxX
            if (type1ParticlePosition[1] > maxY):
                type1ParticlePosition[1] = maxY
            if (type1ParticlePosition[0] < minX):
                type1ParticlePosition[0] = minX
            if (type1ParticlePosition[1] < minY):
                type1ParticlePosition[1] = minY
            type1File.write(str(timeStep) + "\t" + str(type1ParticlePosition[0]) + "\t" + str(type1ParticlePosition[1]) + "\n")

        if(((type2ParticlePosition[0] < maxX) and (type2ParticlePosition[0] > minX)) and ((type2ParticlePosition[1] < maxY) and (type2ParticlePosition[1] > minY))):
            type2File.write(str(timeStep) + "\t" + str(type2ParticlePosition[0]) + "\t" + str(type2ParticlePosition[1]) + "\n")
            if(np.random.random() < randomMovementChance):
                randomTheta = 2.0*np.pi*np.random.random()
                type2ParticlePosition = np.add(type2ParticlePosition , np.array([np.cos(randomTheta), np.sin(randomTheta)]))
            else:
                type2ParticlePosition = np.add(type2ParticlePosition , particleType2Force(type2ParticlePosition))
        elif(not type1OutOfBounds):
            type1OutOfBounds = True
            if (type1ParticlePosition[0] > maxX):
                type1ParticlePosition[0] = maxX
            if (type1ParticlePosition[1] > maxY):
                type1ParticlePosition[1] = maxY
            if (type1ParticlePosition[0] < minX):
                type1ParticlePosition[0] = minX
            if (type1ParticlePosition[1] < minY):
                type1ParticlePosition[1] = minY
            type2File.write(str(timeStep) + "\t" + str(type2ParticlePosition[0]) + "\t" + str(type2ParticlePosition[1]) + "\n")